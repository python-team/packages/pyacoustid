pyacoustid (1.2.2-2) UNRELEASED; urgency=medium

  * Fix field name typo in debian/upstream/metadata (Repository-Browser ⇒
    Repository-Browse).
  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 16 Nov 2022 13:45:00 -0000

pyacoustid (1.2.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * New upstream release
  * Use the new Debian Python Team contact name and address
  * debian/watch
    - track github releases
  * debian/{control, copyright}
    - update homepage URL
  * debian/copyright
    - extend packaging copyright years

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 13.
  * Set field Upstream-Contact in debian/copyright.
  * Set upstream metadata fields: Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Sat, 09 Oct 2021 21:24:39 -0400

pyacoustid (1.2.0-2) unstable; urgency=medium

  * debian/tests/fingerprint-calculation
    - dont run python2 tests; Closes: #968839

 -- Sandro Tosi <morph@debian.org>  Sat, 22 Aug 2020 13:56:28 -0400

pyacoustid (1.2.0-1) unstable; urgency=medium

  * New upstream release
  * debian/copyright
    - update upstream copyright notice
    - extend packaging copyright years
  * debian/control
    - run wrap-and-sort
    - bump Standards-Version to 4.5.0 (no changes needed)

 -- Sandro Tosi <morph@debian.org>  Fri, 24 Jul 2020 23:28:37 -0400

pyacoustid (1.1.5-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Sandro Tosi ]
  * Drop python2 support; Closes: #937383

 -- Sandro Tosi <morph@debian.org>  Thu, 12 Dec 2019 20:59:57 -0500

pyacoustid (1.1.5-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces
  * Convert git repository from git-dpm to gbp layout

  [ Sandro Tosi ]
  * New maintainer; Closes: 889775
  * New upstream release; Closes: #878623, #881565
  * debian/control
    - bump Standards-Version to 4.2.1 (no changes needed)

 -- Sandro Tosi <morph@debian.org>  Fri, 23 Nov 2018 18:24:26 -0500

pyacoustid (1.1.2-2) unstable; urgency=medium

  * Team upload.
  * debian/control: Change libchromaprint0 to libchromaprint1. (Closes:
    #836237)

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 03 Sep 2016 09:21:20 +0200

pyacoustid (1.1.2-1) unstable; urgency=medium

  [ Stefano Rivera ]
  * Team upload.
  * New upstream release.
  * Drop patches, superseded upstream.
  * Bump debhelper compat level to 9.
  * Bump Standards-Version to 3.9.8, no changes needed.
  * Switch watch file to pypi.debian.net.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

 -- Stefano Rivera <stefanor@debian.org>  Tue, 23 Aug 2016 14:22:53 -0700

pyacoustid (1.1.0-3) unstable; urgency=medium

  * Force the locale to C.UTF-8 everywhere (Closes: #764979)

 -- Simon Chopin <chopin.simon@gmail.com>  Sun, 12 Oct 2014 21:59:59 +0200

pyacoustid (1.1.0-2) unstable; urgency=medium

  * Force the locale to C.UTF-8 when invoking dh_auto_clean (Closes: #764979)

 -- Simon Chopin <chopin.simon@gmail.com>  Sun, 12 Oct 2014 21:56:20 +0200

pyacoustid (1.1.0-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards version to 3.9.6 (no changes needed)
  * Update copyright years
  * Add a Python 3 package:
    + Transition to pybuild
    + Add a patch to setup.py to handle non-UTF-8 locales

 -- Simon Chopin <chopin.simon@gmail.com>  Tue, 30 Sep 2014 18:30:56 +0200

pyacoustid (1.0.0-1) unstable; urgency=low

  [ Simon Chopin ]
  * New upstream release.
  * Bump Standards version to 3.9.4
    + B-D on debhelper >= 8.1 for build-arch and build-indep targets

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

 -- Simon Chopin <chopin.simon@gmail.com>  Tue, 14 May 2013 22:08:38 +0200

pyacoustid (0.7-1) unstable; urgency=low

  * New upstream release.

 -- Simon Chopin <chopin.simon@gmail.com>  Fri, 01 Jun 2012 13:37:55 +0200

pyacoustid (0.6-1) unstable; urgency=low

  * New upstream release
    - Drop the backported patch.
  * Bump the Standards-Version to 3.9.3
  * Use the Copyright format 1.0 URL in d/copyright

 -- Simon Chopin <chopin.simon@gmail.com>  Thu, 05 Apr 2012 18:11:42 +0200

pyacoustid (0.4-1) unstable; urgency=low

  * New upstream release
  * Patch: 2.6 compatibility backported from upstream

 -- Simon Chopin <chopin.simon@gmail.com>  Mon, 20 Feb 2012 00:06:58 +0200

pyacoustid (0.3-1) unstable; urgency=low

  * Initial release (Closes: #651094)

 -- Simon Chopin <chopin.simon@gmail.com>  Sat, 24 Dec 2011 17:42:15 +0200
